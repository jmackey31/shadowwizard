function jmshadowwizard(options) {
    let images = document.querySelectorAll('.shadow-wizard');

    if (options.shadow_type === 'hard') {
        options.shadow_type = '0px';
    } else {
        options.shadow_type = '15px';
    }

    images.forEach(image => {
        const box_shadow_styling = `10px 10px ${options.shadow_type} 1px rgba(0,0,0,0.12)`;
        image.style.boxShadow = box_shadow_styling;

        if (options.padding) {
            image.style.padding = '1em';
        }
    });
}

module.exports.jmshadowwizard = jmshadowwizard;
