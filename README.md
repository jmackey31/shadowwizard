# JM's Simple NPM Package Example (css shadow styles).

Add css shadows to your project with this package.

# Installation

`npm i jmshadowwizard --save`

## How To Use It?

import { jmshadowwizard } from 'jmshadowwizard';

jmshadowwizard({
    shadow_type: 'soft',
    padding: false
});

## Options

JM Shadow Wizard supports 2 options, both of which are optional:

* *shadow_type* - _hard | soft_ (Defaults to soft)
* *padding* - _boolean_ (Defaults to false)
